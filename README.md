# Projet Colo



Projet de Colocation

Bienvenue dans mon projet. Ce projet vise à faciliter la gestion des tâches et des dépenses entre les colocataires, en offrant un tableau de bord interactif, des fonctionnalités de connexion et d'inscription, ainsi que des interfaces pour gérer les tâches et les dépenses.
Technologies Utilisées

    JavaScript : Pour la logique de l'application côté client.
    SQL : Pour la gestion de la base de données.
    PHP : Pour la logique de l'application côté serveur et les interactions avec la base de données.
    HTML : Pour la structure des pages web.
    CSS : Pour le style des pages web.
    Axios : Pour les requêtes HTTP côté client.
    nginx: coté back

Fonctionnalités

    Login et Register : Les utilisateurs peuvent créer un compte et se connecter.
    Gestion des Tâches : Ajouter, afficher, modifier et supprimer des tâches.
    Gestion des Dépenses : Ajouter, afficher, modifier et supprimer des dépenses.
    Tableau de Bord : Vue d'ensemble des tâches et des dépenses.

Problèmes Connus

    Erreur de Récupération des Données dans le Tableau de Bord :
        Actuellement, il y a une erreur lors de la récupération des tâches et des dépenses via les requêtes GET.

    Problème de Connexion :
        Un problème persiste avec le système de connexion. le login ne s'affiche pas et je n'arrive pas à me connecter au back

Dépendances

    Axios : Utilisé pour les requêtes HTTP côté client.